import random

import numpy as np
import torch
from torch.utils.data import Dataset

from variant_full_nids import cat_dict, generate_result, test_dataset, device

np.random.seed(12345)
torch.manual_seed(12345)
random.seed(12345)


class dataset_test_fgsm(Dataset):

    def __init__(self, x, y):
        self.x = x
        self.y = y
        bin_pred_y = np.zeros(self.y.shape).astype(np.int64)
        for i in range(len(y)):
            if y[i] == cat_dict["Normal"]:
                bin_pred_y[i] = 0
            else:
                bin_pred_y[i] = 1
        self.y = bin_pred_y

    def __len__(self):
        return self.x.shape[0]

    def __getitem__(self, item):
        return self.x[item], self.y[item]


def get_test_loader(num_holdout_samples, num_test_samples):
    test_x = test_dataset.get_x()
    test_y = test_dataset.get_y()

    test_total = np.concatenate([test_x, np.expand_dims(test_y, axis=1)], axis=1)
    np.random.shuffle(test_total)

    test_x_holdout = test_total[:num_holdout_samples, :-1]
    test_y_holdout = test_total[:num_holdout_samples, -1]

    test_x_test = test_total[num_holdout_samples:num_test_samples + 1, :-1]
    test_y_test = test_total[num_holdout_samples:num_test_samples + 1, -1]

    return torch.utils.data.DataLoader(dataset_test_fgsm(test_x_holdout, test_y_holdout), batch_size=1,
                                       shuffle=True), torch.utils.data.DataLoader(
        dataset_test_fgsm(test_x_test, test_y_test), batch_size=1, shuffle=True), test_x.shape[1], int(
        np.max(test_y) + 1)


class BlackBoxWrapper:
    def __init__(self, input_dim, output_dim, label_ratio=0.1, path_to_model='models'):
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.label_ratio = label_ratio
        self.path_to_model = path_to_model

    def __call__(self, x):
        x = x.cpu().detach().numpy()
        x = np.array(x).astype(np.float)
        y = self.getBinPrediction(x)
        y = torch.LongTensor(y).to(device)
        return y

    def eval(self):

        return

    def getBinPrediction(self, x):
        x = np.array(x).astype(np.float)
        pred_Y = generate_result(x, label_ratio_=self.label_ratio, path_to_model=self.path_to_model)
        bin_pred_y = []
        for y in pred_Y:
            if y == cat_dict["Normal"]:
                bin_pred_y.append(0)
            else:
                bin_pred_y.append(1)
        return np.array(bin_pred_y)

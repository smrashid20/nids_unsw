from __future__ import print_function

import random

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

from blackbox_fgsm_utils import get_test_loader, BlackBoxWrapper


np.random.seed(12345)
torch.manual_seed(12345)
random.seed(12345)


netB_train_num = 150  # the number of training data for the F network
epsilon = 0.3  # data sensitivity for adversarial attack
lamb = 0.1  # data augumentation step size
num = netB_train_num + 1  # New dictionary's key count
iter_epoch = 10
sub_epoch = 6
epoch_num = iter_epoch * sub_epoch  # total epoch. the count will be a (substitute epoch*iteration epoch).
tau = 1  # lamda change period
train_flag = 1  # if you want to train the F network, set the value 1

# After sigma epoch, the number of augmentation data will reduce through k_num
sigma = 2
sigma = sigma + 1
k_num = 400
use_cuda = False

test_loader_holdout, test_loader_test, input_dim, output_dim = get_test_loader(num_holdout_samples=netB_train_num,
                                                                               num_test_samples=1000)


def weight_reset(m):
    if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
        m.reset_parameters()


def fgsm_attack(image, epsilon, data_grad):
    sign_data_grad = data_grad.sign()
    perturbed_image = image + epsilon * sign_data_grad
    perturbed_image = torch.clamp(perturbed_image, 0, 1)
    return perturbed_image


def sampling_func(lou, sigma, k_num, Dict_len):
    N = int(round(k_num * (lou - sigma)))
    A_list = list(range(1, Dict_len + 1))
    index = np.random.choice(A_list, N, replace=False)
    # return is array
    return index


class Substitute_Network(nn.Module):
    def __init__(self, n_input=192, n_output=10):
        super(Substitute_Network, self).__init__()
        self.fc1 = nn.Linear(n_input, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64, 32)
        self.fc4 = nn.Linear(32, n_output)

    def forward(self, x):
        out_1 = torch.relu(self.fc1(x))
        out_2 = torch.relu(self.fc2(out_1))
        out_3 = torch.relu(self.fc3(out_2))
        out_4 = self.fc4(out_3)

        return F.log_softmax(out_4, dim=1)


net_A = BlackBoxWrapper(input_dim, output_dim, 0.5)
net_B = Substitute_Network(input_dim,output_dim)

criterion = nn.CrossEntropyLoss()
optimizer_net_B = optim.Adam(net_B.parameters(), lr=0.001)

device = "cpu"

model_B = net_B

Data_Dict = {}
Label_Dict = {}

for i, data in enumerate(test_loader_holdout, 0):
    inputs, labels = data
    inputs, labels = inputs, labels
    j = i + 1
    Data_Dict[j] = np.array(inputs).astype(np.float)
    Label_Dict[j] = labels
    if j == netB_train_num:
        break

if train_flag:
    for sub_num in range(sub_epoch):
        model_B.apply(weight_reset)
        Aug_Dict = {}
        y_dict = {}
        lou = sub_num

        for iter_num in range(iter_epoch):
            running_loss = 0.0
            for k in Data_Dict.keys():
                y_A = net_A(torch.FloatTensor(Data_Dict[k]))
                data = torch.FloatTensor(Data_Dict[k])

                optimizer_net_B.zero_grad()
                outputs = model_B(data)
                loss = F.nll_loss(outputs.cpu(), y_A.cpu())

                loss.backward(retain_graph=True)
                optimizer_net_B.step()

                running_loss += loss.item()

            print('subtitute_epoch : ', sub_num + 1, ' iteration_epoch : ', iter_num + 1, 'LOSS : ',
                  running_loss / float(len(Data_Dict)))

        # Data Augmentation
        print('Data_Augmentation')
        if lou >= sigma:
            sampling_list = sampling_func(lou, sigma, k_num, len(Data_Dict))
            for k in sampling_list:
                y_A = net_A(torch.FloatTensor(Data_Dict[k]))
                data = Variable(torch.FloatTensor(Data_Dict[k]), requires_grad=True)

                output = model_B(data)
                loss = F.nll_loss(output.cpu(), y_A.cpu())

                loss.backward()

                data_grad = data.grad.data
                perturbed_data = fgsm_attack(data, lamb * (-1) ** int(sub_num / tau), data_grad)

                Aug_Dict[num] = perturbed_data.clone()
                num += 1
        elif lou < sigma:
            for k in Data_Dict.keys():
                y_A = net_A(torch.FloatTensor(Data_Dict[k]))
                data = Variable(torch.FloatTensor(Data_Dict[k]), requires_grad=True)

                output = model_B(data)
                loss = F.nll_loss(output.cpu(), y_A.cpu())

                loss.backward()
                data_grad = data.grad.data
                perturbed_data = fgsm_attack(data, lamb * (-1) ** int(sub_num / tau), data_grad)
                Aug_Dict[num] = perturbed_data.clone()
                num += 1

        print('Data_Dict length : ', len(Data_Dict))
        Data_Dict.update(Aug_Dict)
    torch.save(net_B.state_dict(), 'net_B.pth')
    print('Finished Training network_B for black box adversarial attack ')

model_B.load_state_dict(torch.load('net_B.pth'))
print('load pretrained based network_B data')


# test for the result which is compared  O(x') with real labels.
def test(model_B, epsilon):
    count = 0
    sumA = 0
    sumB = 0
    correct = 0
    trans = 0

    for i, data in enumerate(test_loader_test, 0):
        inputs, labels = data
        inputs, labels = np.array(inputs).astype(np.float), torch.LongTensor(np.array(labels).astype(np.int64))
        inputs = Variable(torch.FloatTensor(inputs))
        inputs.requires_grad = True

        output = model_B(inputs)
        loss = F.nll_loss(output.cpu(), labels)

        model_B.zero_grad()
        loss.backward()
        inputs_grad = inputs.grad.data

        perturbed_data = fgsm_attack(inputs, epsilon, inputs_grad)
        output = net_A(torch.FloatTensor(perturbed_data))

        final_pred = output.cpu().detach().numpy()
        if final_pred.item() == labels.item():
            correct += 1

    model_B.eval()
    for i, data in enumerate(test_loader_test, 0):
        inputs, labels = data
        inputs, labels = np.array(inputs).astype(np.float), labels
        inputs = Variable(torch.FloatTensor(inputs))

        count += 1
        y_A = net_A(torch.FloatTensor(inputs))
        if y_A.cpu().data == labels.data:
            sumA += 1

        result_vecB = model_B(inputs)
        y_B = result_vecB.max(1)[1]

        if y_B.data == labels.data:
            sumB += 1
        if y_B.data == y_A.cpu().data:
            trans += 1
    print('\n \n eps : ', epsilon)
    print('accuracy model A : ', sumA / count)
    print('accuracy  model B : ', sumB / count)
    print('accuracy model, A = B : ', trans / count)
    print('Adversarial acc A : ', correct / count)


print('test ....')
test(model_B, epsilon)
